from drb.exceptions.core import DrbException


class DrbTarNodeException(DrbException):
    pass
