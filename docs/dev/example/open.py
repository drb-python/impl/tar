from drb.drivers.file import DrbFileNode
from drb.drivers.tar.base_node import DrbBaseTarNode
from drb.drivers.tar.node import DrbTarAttributeNames
from tarfile import ExFileObject


path = 'path/to/a/container.zip'

baseNode = DrbFileNode(path)
tarNode = DrbBaseTarNode(baseNode)

# Access a children with his name
tarNode['subFile']
tarNode['subDirectory']['subFile']

# Check if a file has children
tarNode['subDirectory']['subFile'].has_child()

# Check if a specific child his present
tarNode.has_child(name='subDirectory')

# Get the first child
tarNode[0]

# Get all the attributes of the zip node
tarNode.attributes

# search for a specific attributes
tarNode.get_attribute(name=DrbTarAttributeNames.DIRECTORY.value)

# Read the content of a file in the container
tarNode['subFile'].get_impl(ExFileObject)
