.. _example:

Examples
=========

Open and read a tar container
-----------------------------
.. literalinclude:: example/open.py
    :language: python
