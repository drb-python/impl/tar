.. _install:

Installation of tar implementation
====================================
Installing ``drb-driver-tar`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-tar
