===================
Data Request Broker
===================
---------------------------------
TAR driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-tar/month
    :target: https://pepy.tech/project/drb-driver-tar
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-tar.svg
    :target: https://pypi.org/project/drb-driver-tar/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-tar.svg
    :target: https://pypi.org/project/drb-driver-tar/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-tar.svg
    :target: https://pypi.org/project/drb-driver-tar/
    :alt: Python Version Support Badge

-------------------

This drb-driver-tar module implements access to tar containers with DRB data model.
It is able to navigates among the tar contents.


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

